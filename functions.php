<?php 
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles');
function theme_enqueue_styles() {
    wp_dequeue_style( 'main-stylesheet' );
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style('child-style', get_stylesheet_uri(), array('swift_custom_styles') );
    
    wp_enqueue_style('font-style','/wp-content/themes/goodwin-update/fonts/goodwin-fonts.css' , array('swift_custom_styles') );
}

//make excerpt shorter to make up for bigger font
function custom_excerpt_length2( $length ) {
	return 35;
}
add_filter( 'excerpt_length', 'custom_excerpt_length2', 1001);
?>



<?php

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:
         
// if ( !function_exists( 'child_theme_configurator_css' ) ):
//     function child_theme_configurator_css() {
//         wp_enqueue_style( 'chld_thm_cfg_child', trailingslashit( get_stylesheet_directory_uri() ) . 'style.css', array( 'parent-style' ) );
//     }
// endif;
// add_action( 'wp_enqueue_scripts', 'child_theme_configurator_css' );

// END ENQUEUE PARENT ACTION

